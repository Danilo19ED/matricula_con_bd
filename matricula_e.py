import sqlite3


class Estudiantes:
    id_estd=""
    nombre=""
    email=""


    def __init__(self):
        self.conexion = sqlite3.connect('Matricula.sqlite')
        self.cursor= None
        print("Conecxion establecida !!!")

    def crear_db(self):
        sql_registro = """CREATE TABLE registro (
            id_estd INTEGER NOT NULL PRIMARY KEY UNIQUE,
            nombre TEXT UNIQUE,
           email TEXT
           ) """

        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_registro)
        print("Tabla creada!!! ")
        self.cursor.close()

    def Matricula_estudiante(self):
        print("registro de estudiante")

        id_estd = str(input("ingrese identificación: "))
        nombre = str(input("ingrese nombre: "))
        email = str(input("ingrese el email: "))


        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO registro(id_estd, nombre, email) VALUES (?, ?, ?)",
                                [(id_estd, nombre, email),
                                 ])
        self.conexion.commit()
        self.cursor.close()
        print("Resgistro completo!!!")

    def Visualizar_datos(self):
        print("Listado de alumnos")
        sql = """SELECT*FROM registro"""
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql)
        filas = self.cursor.fetchall()
        for f in filas:
            print(f)
        self.cursor.close()

    def Consultar_estd(self):

        print("consulta por id de los estudiantes")

        id_estd = str(input("ingrese el id del estudiante a buscar: "))

        self.cursor = self.conexion.cursor()
        self.cursor.execute("SELECT * FROM registro WHERE id_estd = (?)", (id_estd,))
        rows = self.cursor.fetchall()

        for f in rows:
            print(f)
        print("proceso realizado")

        self.cursor.close()

    def Eliminar_estd(self):
        id_estd = input('Ingrese el número de cédula del estudiante a eliminar: ')
        self.cursor = self.conexion.cursor()
        self.cursor.execute("DELETE FROM registro WHERE id_estd = (?)", (id_estd,))
        self.conexion.commit()
        #self.conexion.close()
        print("estudiante eliminado")

    def Salir(self):
        print("El proceso a terminado")


    def menu_opciones(self):
        op=0
        t_r=5

        while op != t_r:
            print("Menú")
            print("1. Matricular estudiante")
            print("2. Visualizar datos")
            print("3. Buscar estudiante")
            print("4. Eliminar estudiante")
            print("5. Salir")

            op = int(input("ingrese opción: "))

            if op == 1:
                self.Matricula_estudiante()

            elif op == 2:
                self.Visualizar_datos()

            elif op == 3:
                self.Consultar_estd()

            elif op == 4:
                self.Eliminar_estd()

            elif op == 5:
                self.Salir()

            else:
                print(" número ingresado fuera de rango")
                print("ingrese un número del rango 1 a 5")



if __name__ == "__main__":
    inicio = Estudiantes()
    #inicio.crear_db()
    inicio.menu_opciones()
